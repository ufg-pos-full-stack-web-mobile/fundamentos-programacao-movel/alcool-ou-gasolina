package com.example.bruno.alcoolougasolina;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by bruno on 11/02/18.
 */

public class ButtonVerifyListener implements View.OnClickListener {

    private final TextView textResult;
    private final EditText editPriceGas;
    private final EditText editPriceAlchool;

    public ButtonVerifyListener(TextView textResult, EditText editPriceGas, EditText editPriceAlchool) {
        this.textResult = textResult;
        this.editPriceGas = editPriceGas;
        this.editPriceAlchool = editPriceAlchool;
    }

    @Override
    public void onClick(View view) {
        final String priceGasStr = editPriceGas.getText().toString();
        final String priceAlchoolStr = editPriceAlchool.getText().toString();

        final Double priceGas = Double.parseDouble(priceGasStr);
        final Double priceAlchool = Double.parseDouble(priceAlchoolStr);

        final String msgResult = view.getContext().getString(R.string.msg_best_comb_template);

        final Double result = priceAlchool / priceGas;

        String comb = null;
        if (0.7 <= result) {
            comb = view.getContext().getString(R.string.text_gas);
        } else {
            comb = view.getContext().getString(R.string.text_alchool);
        }

        textResult.setText(String.format(msgResult, comb));
    }
}
