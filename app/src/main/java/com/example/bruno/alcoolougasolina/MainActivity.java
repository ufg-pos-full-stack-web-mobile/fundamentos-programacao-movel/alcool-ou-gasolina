package com.example.bruno.alcoolougasolina;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText editPriceGas;
    private EditText editPriceAlchool;
    private Button buttonVerify;
    private TextView textResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editPriceGas = findViewById(R.id.edit_price_gas);
        editPriceAlchool = findViewById(R.id.edit_price_alchool);
        buttonVerify = findViewById(R.id.button_verify);
        textResult = findViewById(R.id.text_result);

        buttonVerify.setOnClickListener(new ButtonVerifyListener(textResult, editPriceGas, editPriceAlchool));
    }
}
